﻿using System;
using ChuckSample.Core.Data;
using ChuckSample.Core.Data.Implementation;

namespace ChuckSample.Core
{
	public class App
	{
		public static readonly string ApiRoot = "http://api.icndb.com/";

		private static IJokeRepository _jokeRepository;
		public static IJokeRepository JokeRepository
		{
			get
			{
				if (_jokeRepository == null)
					_jokeRepository = new JokeRepository();
				return _jokeRepository;
			}
		}

	}
}
