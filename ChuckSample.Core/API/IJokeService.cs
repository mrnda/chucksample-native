﻿
using System.Threading.Tasks;
using ChuckSample.Core.API.Model;
using ChuckSample.Core.Model;

namespace ChuckSample.Core.API
{
	public interface IJokeService
	{
		Task<ApiResponse<JokeModel>> GetRandomJoke();
	}
}
