﻿using System;
namespace ChuckSample.Core.API.Model
{
	public class ApiResponse<T>
	{
		public String Type
		{
			get;
			set;
		}

		public T Value
		{
			get;
			set;
		}
	}
}
