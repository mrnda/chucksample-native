﻿using System;
using System.Threading.Tasks;
using ChuckSample.Core.API.Model;
using ChuckSample.Core.Model;
using RestSharp.Portable;
using RestSharp.Portable.HttpClient;

namespace ChuckSample.Core.API.Implementation
{
	public class JokeService : IJokeService
	{
		public async Task<ApiResponse<JokeModel>> GetRandomJoke()
		{
			using (var restClient = new RestClient(App.ApiRoot))
			{
				var request = new RestRequest("jokes/random");

				var response = await restClient.Execute<ApiResponse<JokeModel>>(request);

				return response.Data;
			}
		}
	}
}
