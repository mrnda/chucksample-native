﻿using System;
using System.Threading.Tasks;
using ChuckSample.Core.API;
using ChuckSample.Core.API.Implementation;
using ChuckSample.Core.Model;

namespace ChuckSample.Core.Data.Implementation
{
	public class JokeRepository : IJokeRepository
	{
		private IJokeService _jokeService;
		public IJokeService JokeService
		{
			get
			{
				if (_jokeService == null)
					_jokeService = new JokeService();
				return _jokeService;
			}
			
		}

		public async Task<JokeModel> GetRandomJoke()
		{
			var joke = await JokeService.GetRandomJoke();
			return joke.Value;
		}
	}
}
