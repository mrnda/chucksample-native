﻿using System;
using System.Threading.Tasks;
using ChuckSample.Core.Model;

namespace ChuckSample.Core.Data
{
	public interface IJokeRepository
	{
		Task<JokeModel> GetRandomJoke();
	}
}
