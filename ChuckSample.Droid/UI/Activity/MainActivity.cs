﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ChuckSample.Droid.UI.Activity.Base;
using ChuckSample.Droid.UI.Fragment;

namespace ChuckSample.Droid.UI.Activity
{
	[Activity(Label = "Chuck Sample", MainLauncher = true)]
	public class MainActivity : BaseActivity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.ActivityMain);
			SetupView();
		}

		void SetupView()
		{
			FragmentManager.BeginTransaction()
						   .Replace(Resource.Id.container_main_activity, JokeFragment.NewInstance())
						   .Commit();
		}
	}
}
