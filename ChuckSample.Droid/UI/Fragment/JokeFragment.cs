﻿

using System;
using System.Threading;
using Android.OS;
using Android.Util;
using Android.Views;
using Android.Widget;
using ChuckSample.Core;
using ChuckSample.Droid.UI.Fragment.Base;
using Com.Lilarcor.Cheeseknife;

namespace ChuckSample.Droid.UI.Fragment
{
	public class JokeFragment : BaseFragment
	{

		public static JokeFragment NewInstance()
		{
			var instance = new JokeFragment();
			return instance;
		}


		[InjectView(Resource.Id.joke_text)]
		TextView JokeText;



		[InjectOnClick(Resource.Id.joke_button)]
		async void OnDownloadClicked(object sender, EventArgs e)
		{
			var jokeTask = App.JokeRepository.GetRandomJoke();

			JokeText.Text = "Downloading...";

			var joke = await jokeTask;

			JokeText.Text = joke.Joke;
		}


		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate(Resource.Layout.FragmentJoke, container, false);
			Cheeseknife.Inject(this, view);
			return view;
		}

	}
}
